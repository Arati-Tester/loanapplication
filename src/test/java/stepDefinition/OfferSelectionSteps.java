package stepDefinition;

import Base.TestBase;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.OffersPage;

public class OfferSelectionSteps {

    public WebDriver driver;
    public  OffersPage offer;

    public OfferSelectionSteps() {

        this.driver = TestBase.getDriver(); // Initialize WebDriver
        this.offer = new OffersPage(driver); // Initialize offerpage object
    }

    @When("user click on any generated offer")
    public void user_click_on_any_generated_offer() {


        offer.selectOffer();
    }

    @Then("Action required page will be open")
    public void action_required_page_will_be_open() {

        offer.ActionRequirePage();
    }


    @When("user upload a all required {string} and {string}")
    public void userUploadAAllRequiredAnd(String FileType, String FileName) throws InterruptedException {

        offer.uploadRequiredDocs(FileType, FileName);
    }

    @Then("user again navigate to same offer page")
    public void user_again_navigate_to_same_offer_page() throws InterruptedException {


        offer.navigateBackToOffers();
    }

    @When("user again select a offer")
    public void user_again_select_a_offer() throws InterruptedException {

        offer.againOfferSelect();
    }

    @Then("Confirmation Pop-up will be open & confirm the same")
    public void confirmation_pop_up_will_be_open_confirm_the_same() throws InterruptedException {

        offer.offerConfirmation();
    }

    @Then("User navigate to KYC details")
    public void user_navigate_to_kyc_details() throws InterruptedException {

        offer.navigateToKYC();


    }


    @When("User click on initiate KYC")
    public void userClickOnInitiateKYC() throws InterruptedException {

        offer.initiateKYC();
    }

    @Then("User navigate to KYC page")
    public void userNavigateToKYCPage() throws InterruptedException {

        offer.navigateToKYCStatus();
    }

    @When("user navigate back to KYC Status page And click on KYC Status")
    public void userNavigateBackToKYCPageAndClickOnKYCStatus() throws InterruptedException {

//window handle concept is here
        offer.kycStatus();
        
    }

    @Then("user navigate to loan aggrement page")
    public void userNavigateToLoanAggrementPage() {

        offer.landOnAgreement();

    }

    @When("user click on Accept agreement & continue")
    public void userClickOnAcceptAgreementContinue() throws InterruptedException {

        offer.acceptAgreement();
    }

    @Then("user should get a OTP pop-up")
    public void userShouldGetAOTPPopUp() throws InterruptedException {

        offer.otpPopUp();
    }

    @And("user confirm the agreement")
    public void userConfirmTheAgreement() {


        offer.confirmAgreement();
    }
    @And("user landed on repayment plan page")
    public void userLandedOnRepaymentPlanPage() throws InterruptedException {

      offer.repaymentPlanPage();

    }


}
