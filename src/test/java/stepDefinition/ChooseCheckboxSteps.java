package stepDefinition;

import Base.TestBase;
import io.cucumber.java.AfterStep;
import io.cucumber.java.JavaBackendProviderService;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import pages.CheckboxCLick;

public class ChooseCheckboxSteps {


    public WebDriver driver;
    public CheckboxCLick check;


    public ChooseCheckboxSteps() {

        this.driver = TestBase.getDriver(); // Initialize WebDriver
        this.check = new CheckboxCLick(driver); // Initialize checkbox object
    }


    @When("user scroll down the page")
    public void user_scroll_down_the_page() throws InterruptedException {

        check.scrollDown();
//        Thread.sleep(2000);
//        check.scrollDown();

    }

    @When("user check the checkboxes of guarantor & applicant")
    public void user_check_the_checkboxes_of_guarantor_applicant() throws InterruptedException {

        check.markCheckbox();

    }

    @When("user click on Save button")
    public void user_click_on_save_button() throws InterruptedException {

        check.saveAndContinue();

    }

    @Then("user should navigate to review page")
    public void user_should_navigate_to_review_page() throws InterruptedException {


        check.reviewPage();
    }

    @When("user click on submit application button")
    public void user_click_on_submit_application_button() throws InterruptedException {



        check.submitApplication();
    }

    @Then("user should navigate to consent handle page")
    public void user_should_navigate_to_consent_handle_page() throws InterruptedException {

        Thread.sleep(3000);
        check.verifyConsentPage();
    }

}
