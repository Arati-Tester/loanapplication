package stepDefinition;

import Base.TestBase;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import pages.SubmitLoanApp;
import pages.UploadPage;

public class UploadDocsSteps {


    public WebDriver driver;
    public UploadPage upload;


    public UploadDocsSteps() {

        this.driver = TestBase.getDriver(); // Initialize WebDriver
        this.upload = new UploadPage(driver); // Initialize upload object
    }


    @When("User click on button InitiateLoanApplication")
    public void user_click_on_button_initiate_loan_application() {

        upload.startLoanJourney();
    }

    @Then("upload documents page should get open")
    public void upload_documents_page_should_get_open() throws InterruptedException {

        upload.validateDocumentPage();
    }

    @When("user clicks on doc Type dropdown and select a value PAN to {string}")
    public void user_clicks_on_doc_type_dropdown_and_select_a_value_pan_to(String UploadFileType) throws InterruptedException {

        upload.selectDocumentType(UploadFileType);
    }

    @When("user click on a browse to upload a file {string}")
    public void user_click_on_a_browse_to_upload_a_file(String UploadFile) throws InterruptedException {

        upload.uploadFile(UploadFile);
    }

    @Then("user click on upload button")
    public void userClickOnUploadButton() throws InterruptedException {

        upload.clickUpload();

    }


    @When("user click on dropdown for {string}")
    public void userClickOnDropdownFor(String fileType) throws InterruptedException {

        upload.borrowerSelectFileType(fileType);

    }

    @And("he click on browse to upload a {string}")
    public void heClickOnBrowseToUploadA(String file) throws InterruptedException {

        upload.borrowerUploadFile(file);
    }


    @Then("user click on upload button of borrower")
    public void userClickOnUploadButtonOfBorrower() throws InterruptedException {

        upload.borrowerUploadbutton();
    }

}
