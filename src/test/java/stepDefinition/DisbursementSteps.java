package stepDefinition;

import Base.TestBase;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.DisbursementPage;

public class DisbursementSteps {


    public static WebDriver driver;
    public  DisbursementPage disburse;

    public DisbursementSteps(){

        this.driver = TestBase.getDriver();
        this.disburse= new DisbursementPage(driver);

    }

    @When("user select a mode of payment")
    public void user_select_a_mode_of_payment() throws InterruptedException {

        disburse.modeOfPayment();
    }



    @When("User enter a account number and confirm the same")
    public void user_enter_a_account_number_and_confirm_the_same() throws InterruptedException {

        disburse.enterAccountNum();
    }

    @When("user enter a IFSC code & account holder name")
    public void user_enter_a_ifsc_code_account_holder_name() throws InterruptedException {

        disburse.addAccountDetails();

    }

    @When("User click on button Initiate Disbursement")
    public void user_click_on_button_initiate_disbursement() throws InterruptedException {

        disburse.startDisbursement();
    }

    @Then("user landed on dashboard page with granted loan successfully")
    public void user_landed_on_dashboard_page_with_granted_loan_successfully() throws InterruptedException {

        disburse.loanGranted();
    }


}
