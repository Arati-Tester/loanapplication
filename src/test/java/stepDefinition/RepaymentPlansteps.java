package stepDefinition;

import Base.TestBase;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.RepaymentPage;

public class RepaymentPlansteps {

    public WebDriver driver;

    public RepaymentPage repay;

    public RepaymentPlansteps(){

        this.driver= TestBase.getDriver();
        this.repay=new RepaymentPage(driver);


    }

    @When("User choose an any repayment plan")
    public void user_choose_an_any_repayment_plan() throws InterruptedException {


        repay.selectRepaymentPlan();
    }

    @When("User click on Confirm plan & proceed button")
    public void user_click_on_confirm_plan_proceed_button() throws InterruptedException {


        repay.proceedWithPlan();
    }

    @Then("User landed on loan disbursement screen")
    public void user_landed_on_loan_disbursement_screen() throws InterruptedException {

        repay.navigateTodisbursementScreen();
    }



}
