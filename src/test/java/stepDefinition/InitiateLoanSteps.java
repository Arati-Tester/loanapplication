package stepDefinition;

import Base.TestBase;
import io.cucumber.java.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import pages.SubmitLoanApp;

import java.io.IOException;

public class InitiateLoanSteps {
    public static WebDriver driver;
    public SubmitLoanApp page;

    @AfterStep
    public static void takeScreenshotAfterStep(Scenario scenario) {
        // Capture screenshot after each step
        byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.attach(screenshot, "image/png", "Step Screenshot");
    }


//    @After
//    public void clearBrowserData() {
////        WebDriver driver = WebDriverManager.getDriver();
//        driver.manage().deleteAllCookies();
//        ((JavascriptExecutor) driver).executeScript("window.localStorage.clear();");
//        ((JavascriptExecutor) driver).executeScript("window.sessionStorage.clear();");
//    }
//    @AfterAll
//    public static void before_or_after_all(){
//
//
//        if (driver != null) {
//            driver.quit();
//        }
//    }

    public InitiateLoanSteps() {

        this.driver = TestBase.getDriver(); // Initialize WebDriver
        this.page = new SubmitLoanApp(driver); // Initialize page object
    }


    @Given("user is on homepage")
    public void user_is_on_homepage() throws IOException, InterruptedException {

        page.openURL();
    }

    @When("user enter a {string} and {string}")
    public void user_enter_a_and(String Amount, String Duration) throws InterruptedException {

        page.addAmountDetails(Amount, Duration);
    }

    @When("user select a {string}")
    public void user_select_a(String ProfileType) {
        page.selectProfile(ProfileType);
    }

    @When("user click on button borrower Details")
    public void user_click_on_button_borrower_details() {
        page.clickBorrower();
    }

    @Then("user should navigate to basic details page")
    public void user_should_navigate_to_basic_details_page() throws InterruptedException {

        page.validateBasicDetailsPage();
    }

    @When("user enter a {string} and {string} and {string} and {string} and {string}")
    public void user_enter_a_and_and_and_and(String FirstName, String Email, String Mobile, String Birthdate, String primaryid) throws InterruptedException {
        page.addBasicDetails(FirstName, Email, Mobile, Birthdate, primaryid);
    }

    @When("user click on button Address")
    public void user_click_on_button_address() {
        page.clickButtonAddress();
    }

    @Then("user successfully landed on address page")
    public void user_successfully_landed_on_address_page() {
        page.validateAddressPage();
    }

    @When("user enter a address details {string} and {string}")
    public void userEnterAAddressDetailsAnd(String pincode, String address) throws InterruptedException {
        page.enterAddress(pincode, address);
    }

    @And("clicking on button Check Eligibilty")
    public void clickingOnButtonCheckEligibilty() throws InterruptedException {

        Thread.sleep(3000);
        page.checkEligibilty();

    }

    @Then("user should navigate to congratulations screen")
    public void user_should_navigate_to_congratulations_screen() throws InterruptedException {

        Thread.sleep(3000);
        page.congratulationsScreen();

    }

}
