package stepDefinition;

import Base.TestBase;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.ConsentPage;

public class ConsentHandleSteps {

    public WebDriver driver;
    public ConsentPage consent;
    public ConsentHandleSteps() {

        this.driver = TestBase.getDriver(); // Initialize WebDriver
        this.consent = new ConsentPage(driver); // Initialize page object
    }

    @When("user click on button Aggregator Selection")
    public void user_click_on_button_aggregator_selection() throws InterruptedException {

        consent.selectAggregator();
    }

    @Then("user navigate to aggregator list page")
    public void user_navigate_to_aggregator_list_page() throws InterruptedException {

        consent.showAggregatorlist();
    }

    @When("user click on aggregator OneMoney")
    public void user_click_on_aggregator_one_money() throws InterruptedException {

        consent.selectOneMoney();
    }

    @When("Enter a aggregator ID")
    public void enter_a_aggregator_id() throws InterruptedException {

        consent.getLoanApplicationID();
        consent.aggregatorID();
    }

    @When("user click on InitiateConsent button")
    public void user_click_on_initiate_consent_button() throws InterruptedException {

        consent.initiateConsent();
    }

    @When("user has to approve consent from API")
    public void user_has_to_approve_consent_from_api() throws InterruptedException {

       consent.approveConsent();
       consent.postApiConsentHandle();

    }

    @Then("user navigate to Offers page")
    public void user_navigate_to_offers_page() throws InterruptedException {

        consent.offersPage();
    }



}
