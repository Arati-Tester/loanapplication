package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.sql.Driver;
import java.util.Objects;

public class UploadPage {

    public WebDriver driver;

    public UploadPage(WebDriver driver) {
        this.driver = driver;
    }


    public void startLoanJourney(){


        driver.findElement(By.xpath("//button[@class='button-css button-naming-css']")).click();
    }

    public void validateDocumentPage() throws InterruptedException {


        Thread.sleep(6000);
       WebElement Label = driver.findElement(By.xpath("//div[@class='title-acccordian'][normalize-space()='Borrower Information']"));

       Label.getText();

       System.out.println(Label.getText());

        Assert.assertEquals(Label.getText(), "Borrower Information");

    }

    public void selectDocumentType(String UploadFileType) throws InterruptedException {

        Thread.sleep(8000);
        WebElement docType= driver.findElement(By.xpath("//div[@class='row']//select[@name='documentType']"));
        Select sc= new Select(docType);
        sc.selectByValue(UploadFileType);
    }


    public void uploadFile(String UploadFile) throws InterruptedException {


        Thread.sleep(3000);
        driver.findElement(By.xpath("//input[@class='file-select form-control']")).sendKeys(UploadFile);

    }


    public void clickUpload() throws InterruptedException {

        Thread.sleep(8000);
        driver.findElement(By.xpath("//div[@class='row']//button[@id='button-addon2']")).click();
        Thread.sleep(3000);
        WebElement fileuploaded =driver.findElement(By.xpath("//td[normalize-space()='Pan']"));
        fileuploaded.getText();
        System.out.println(fileuploaded.getText());

//        if(fileuploaded.getText().equals("PAN")){
//                WebElement docType2=driver.findElement(By.xpath("//div[@class='row']//select[@name='documentType']"));
//                Select sc= new Select(docType2);
//                sc.selectByValue("AADHAAR");
//            Thread.sleep(3000);
//            driver.findElement(By.xpath("//input[@class='file-select form-control']")).sendKeys("/home/techbulls/Pictures/extradots.png");
//            Thread.sleep(3000);
//            driver.findElement(By.xpath("//div[@class='row']//button[@id='button-addon2']")).click();
//
//
//
//        }
    }

    public void borrowerSelectFileType(String fileType) throws InterruptedException {

        Thread.sleep(3000);
        WebElement fileT = driver.findElement(By.xpath("//body/div[@id='root']/div[@class='layout-container']/div[@class='main-sub-content d-flex layout-css']/div[@class='content-container']/main/div/div[@class='col-lg-12']/div[@class='container-responsive']/div[@class='width']/div[@class='width']/div/div[@class='accordion']/div[@class='accordion-item']/div[@class='accordion-collapse collapse show']/div[@class='accordion-body']/div[@class='document-container']/div[@class='card-body']/div[@class='document-type-row']/div[@class='document-type']/select[1]"));
        Select sc1= new Select(fileT);
        sc1.selectByValue(fileType);

    }

    public void borrowerUploadFile(String file) throws InterruptedException {

        Thread.sleep(4000);
        driver.findElement(By.xpath("//input[@class='file-select1 form-control']")).sendKeys(file);


    }

    public void borrowerUploadbutton() throws InterruptedException {

        Thread.sleep(3000);
        driver.findElement(By.xpath("//body/div[@id='root']/div[@class='layout-container']/div[@class='main-sub-content d-flex layout-css']/div[@class='content-container']/main/div/div[@class='col-lg-12']/div[@class='container-responsive']/div[@class='width']/div[@class='width']/div/div[@class='accordion']/div[@class='accordion-item']/div[@class='accordion-collapse collapse show']/div[@class='accordion-body']/div[@class='document-container']/div[@class='card-body']/div[@class='document-type-row']/div[@class='document-type-width']/div[@class='input-group']/button[1]")).click();

    }
}
