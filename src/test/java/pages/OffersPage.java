package pages;

import Utils.CommonFunctions;
import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static io.restassured.RestAssured.given;

public class OffersPage {


    public WebDriver driver;

    public ConsentPage page;

    public String mainWindowHandle;

    private String responseBody;





public OffersPage(WebDriver driver){

    this.driver=driver;

}
public void selectOffer(){
driver.findElement(By.xpath("(//div[@class=\"offer-card-body\"][1])")).click();

}

public void ActionRequirePage(){

    WebElement actionPage =driver.findElement(By.xpath("//h4[@class='mb-0 dashboard_heading']"));
    actionPage.getText();
    Assert.assertEquals(actionPage.getText(), "Action Required");

}

public void uploadRequiredDocs(String FileType, String FileName) throws InterruptedException {

    WebElement option =driver.findElement(By.xpath("//select[@name='documentType']"));
    Select sc= new Select(option);
    sc.selectByValue(FileType);

    driver.findElement(By.cssSelector("input[name='documents']")).sendKeys(FileName);

    driver.findElement(By.xpath("//button[@id='button-addon2']")).click();

    Thread.sleep(3000);

    driver.findElement(By.xpath("//button[normalize-space()='Submit']")).click();

    Thread.sleep(5000);



}

public void navigateBackToOffers() throws InterruptedException {

    Thread.sleep(10000);
    WebElement offerpage =driver.findElement(By.xpath("//div[@class='container-main-heading']"));
    Assert.assertEquals(offerpage.getText(),"Credit Offers");



}

public void againOfferSelect() throws InterruptedException {

    Thread.sleep(3000);
    driver.findElement(By.xpath("//body/div[@id='root']/div[@class='layout-container']/div[@class='main-sub-content d-flex layout-css']/div[@class='content-container']/main/div[@class='col-lg-12']/div[@class='container-final-responsive']/div[@class='offer-first-container']/div[@class='offer-first-container']/div[@class='first-container-gap']/div[@class='width']/form/div[@class='offer-card-container row']/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]")).click();
}

public void offerConfirmation() throws InterruptedException {
    Thread.sleep(3000);

    driver.findElement(By.xpath("//span[@class='button-text']")).click();


}

public void navigateToKYC() throws InterruptedException {

    Thread.sleep(3000);
    WebElement KYC= driver.findElement(By.xpath("//div[@class='kyc-peanding']"));
    KYC.getText();

   Assert.assertEquals(KYC.getText(),"KYC Pending");
}

public void initiateKYC() throws InterruptedException {

     mainWindowHandle = driver.getWindowHandle();
    driver.findElement(By.xpath("//button[normalize-space()='Initiate KYC']")).click();
    Thread.sleep(3000);
}

public void navigateToKYCStatus() throws InterruptedException {


String Kycurl= driver.getCurrentUrl();
Thread.sleep(8000);
Assert.assertEquals(Kycurl, "https://dev.techbulls.io/apps/lsp/initiateKYC");
driver.switchTo().window(mainWindowHandle);

Thread.sleep(10000);

}
public void kycStatus() throws InterruptedException {

//Window handle concept


    Thread.sleep(5000);
    driver.findElement(By.xpath("//button[normalize-space()='Refresh to Check KYC Status']")).click();

Thread.sleep(15000);
    }

    public void landOnAgreement(){

    WebElement aggrement =driver.findElement(By.xpath("//h4[@class='headings3']"));

    Assert.assertEquals(aggrement.getText(), "Congratulations! Your Loan is on the Way");

    }

    public void acceptAgreement() throws InterruptedException {


        CommonFunctions.scrollIntoView(driver.findElement(By.xpath("//span[normalize-space()='Accept Agreement & Continue']")));
//    JavascriptExecutor js= (JavascriptExecutor) driver;
//    js.executeScript("window.scrollBy(0, 1000)");

    Thread.sleep(2000);
   WebElement button= new WebDriverWait(driver, Duration.ofSeconds(3000)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[normalize-space()='Accept Agreement & Continue']")));
   button.click();
//    Thread.sleep(3000);
//    driver.findElement(By.xpath("//span[normalize-space()='Accept Agreement & Continue']")).click();

    }

    public void otpPopUp() throws InterruptedException {

    Thread.sleep(3000);
//    driver.switchTo().alert();

//    Handle Api for OTP


        RestAssured.config = RestAssured.config().sslConfig(SSLConfig.sslConfig().allowAllHostnames());


        String OTPUrl = "https://dev.techbulls.io/apps/lsp/api/bridge/util/getOtp/"+ ConsentPage.LoanApplicationID;


        System.out.print(OTPUrl);
        responseBody = given()
                .when()
                .get(OTPUrl).then()
                .statusCode(200)
                .extract()
                .response()
                .asString(); // Specify endpoint



        // Print response body
        System.out.println("GET Response Body: " + responseBody);

        Thread.sleep(3000);

        driver.findElement(By.xpath("//input[@id='otp-input-0']")).sendKeys(responseBody);

        Thread.sleep(3000);
    }

    public void confirmAgreement(){


        driver.findElement(By.xpath("//span[normalize-space()='Confirm Acceptance']")).click();
    }

    public void repaymentPlanPage() throws InterruptedException {

    Thread.sleep(3000);
   WebElement RepaymentPage = driver.findElement(By.xpath("//div[@class='heading-h4 row']"));
   Assert.assertEquals(RepaymentPage.getText(),"\uD83C\uDF89 Your Loan is Approved - Select Your Repayment Plan!");



    }


}
