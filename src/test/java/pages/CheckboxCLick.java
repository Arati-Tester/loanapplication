package pages;

import Utils.CommonFunctions;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

public class CheckboxCLick {

    public WebDriver driver;

    public CheckboxCLick(WebDriver driver) {

        this.driver= driver;
    }


    public void scrollDown(){

        CommonFunctions.scrollIntoView(driver.findElement(By.xpath("//button[@type='submit']")));

//        JavascriptExecutor js= (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,10000)");



    }

    public void markCheckbox() throws InterruptedException {

        Thread.sleep(3000);
        driver.findElement(By.xpath("//div[@class='accordion-item']//div[2]//span[1]//input[1]")).click();
        driver.findElement(By.xpath("//body//div[@id='root']//div[@class='width']//div[@class='width']//div[@class='width']//div[3]//span[1]//input[1]")).click();

    }

    public void saveAndContinue() throws InterruptedException {

        Thread.sleep(3000);
        driver.findElement(By.xpath("//button[@type='submit']")).click();

    }

    public void reviewPage() throws InterruptedException {

        Thread.sleep(3000);
        WebElement PageName =driver.findElement(By.xpath("//div[@class='progresbar-title'][normalize-space()='Review & Submit']"));

        System.out.println(PageName.getText());

        Assert.assertEquals(PageName.getText(),"Review & Submit");
    }


    public void submitApplication() throws InterruptedException {

        CommonFunctions.scrollIntoView(driver.findElement(By.xpath("//button[@type='submit']")));

//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0, 30000)");

        Thread.sleep(4000);
        driver.findElement(By.xpath(("//button[@type='submit']"))).click();
    }


    public void verifyConsentPage() throws InterruptedException {

//        https://dev.techbulls.io/apps/lsp/aggregatorlist

       String ExpectedURL= driver.getTitle();
       Thread.sleep(3000);
       Assert.assertEquals("Techbulls", ExpectedURL);



    }


}


