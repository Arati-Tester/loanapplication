package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SubmitLoanApp {
    private final WebDriver driver;

    public SubmitLoanApp(WebDriver driver) {
        this.driver = driver;
    }


    public void openURL() throws InterruptedException {
        Thread.sleep(3000);
        driver.get("https://dev.techbulls.io/apps/lsp");
        driver.manage().window().maximize();
        Thread.sleep(3000);
    }

    public void addAmountDetails(String Amount, String Duration) throws InterruptedException {

        Thread.sleep(8000);
        driver.findElement(By.xpath("//input[@placeholder='xxxxxx']")).sendKeys(Amount);
        driver.findElement(By.xpath("//button[@id='input-group-dropdown-2']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//a[normalize-space()='Year']")).click();
//        WebDriverWait wait = new WebDriverWait(driver, java.time.Duration.ofSeconds(50));
//        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("//a[normalize-space()='Year']")));
//        element.click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//input[@placeholder='xx']")).sendKeys(Duration);
    }

    public void selectProfile(String ProfileType) {
        WebElement dropdown = driver.findElement(By.xpath("//select[@name='category']"));
        Select sc = new Select(dropdown);
        sc.selectByValue(ProfileType);
    }

    public void clickBorrower() {
        driver.findElement(By.xpath("//button[@type='submit']")).click();
    }

    public void validateBasicDetailsPage() throws InterruptedException {

        Thread.sleep(3000);
        String title = driver.getTitle();
        Assert.assertEquals(title, "Techbulls");
    }

    public void addBasicDetails(String FirstName, String Email, String Mobile, String Birthdate, String primaryid) throws InterruptedException {
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(FirstName);
        driver.findElement(By.xpath("//input[@placeholder='email@domain.com']")).sendKeys(Email);
        driver.findElement(By.xpath("//input[@name='phone']")).sendKeys(Mobile);
        driver.findElement(By.xpath("//input[@name='dateOfBirth']")).sendKeys(Birthdate);

        driver.findElement(By.xpath(("//button[@id='input-group-dropdown-2']"))).click();
        driver.findElement(By.xpath("//a[normalize-space()='PAN']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//input[@placeholder='xxxx xxxx xxxx xxxx']")).sendKeys(primaryid);

        Thread.sleep(3000);

        // Validate PAN with Primary Id field
        WebElement PANno = driver.findElement(By.xpath("//input[@placeholder='Pan Number']"));
        PANno.click();
        System.out.println(PANno.getAttribute("value"));
        Assert.assertEquals(primaryid, PANno.getAttribute("value"));
    }

    public void clickButtonAddress() {
        driver.findElement(By.xpath("//button[@type='submit']")).click();
    }

    public void validateAddressPage() {
        System.out.println("Welcome To Address Page");
    }

    public void enterAddress(String pincode, String address) throws InterruptedException {

        Thread.sleep(3000);
        driver.findElement(By.name("pinCode")).sendKeys(pincode);
        driver.findElement(By.xpath("//input[@type='address']")).sendKeys(address);
    }

    public void checkEligibilty() {
        driver.findElement(By.xpath("//button[@type='submit']")).click();
    }

    public void congratulationsScreen() {
        System.out.println("Congratulation to initiate your loan application");
    }
}
