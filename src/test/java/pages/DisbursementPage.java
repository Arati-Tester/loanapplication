package pages;

import Utils.CommonFunctions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DisbursementPage {

public final WebDriver driver;

public DisbursementPage(WebDriver driver){

    this.driver=driver;

}

public void modeOfPayment() throws InterruptedException {

    Thread.sleep(3000);
    WebElement radioButton= driver.findElement(By.xpath("//input[@value='ACCOUNT']"));
    if(radioButton.isSelected()){

        WebElement dropdown =driver.findElement(By.xpath("//select[@name='accountType']"));
        Select sc= new Select(dropdown);
        sc.selectByVisibleText("Saving Account");

    }
}

public void enterAccountNum() throws InterruptedException {

    Thread.sleep(3000);
    driver.findElement(By.xpath("//input[@name='accountnumber']")).sendKeys("99776476827");
    Thread.sleep(3000);
    WebElement AccountNum= driver.findElement(By.xpath("//input[@name='accountnumber']"));
    String AccNum = AccountNum.getAttribute("value");

    System.out.print(AccNum);
    Thread.sleep(2000);
    driver.findElement(By.xpath("//input[@name='confirmaccountnumber']")).sendKeys(AccNum);


}

public void addAccountDetails() throws InterruptedException {

    Thread.sleep(3000);
    driver.findElement(By.xpath("//input[@placeholder='IFSC Number']")).sendKeys("IFSC0003456");
    driver.findElement(By.xpath("//input[@placeholder='Account Holder Name']")).sendKeys("Harry");
}

public void startDisbursement() throws InterruptedException {

    CommonFunctions.scrollIntoView(driver.findElement(By.xpath("//button[@type='submit']")));
//    JavascriptExecutor js= (JavascriptExecutor) driver;
//    js.executeScript("window.scrollBy(0, 500)");
    Thread.sleep(3000);

    driver.findElement(By.xpath("//button[@type='submit']")).click();

    }

    public void loanGranted() throws InterruptedException {

     Thread.sleep(20000);
     WebElement dashboard = driver.findElement(By.xpath("//div[@class='congragulations']"));
     Assert.assertEquals(dashboard.getText(),"\uD83C\uDF89 Congratulations, Your Loan is Successfully Disbursed!");
     System.out.println("Loan granted successfully");

    }
}



