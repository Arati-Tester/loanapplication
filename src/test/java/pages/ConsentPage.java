package pages;

import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

import static io.restassured.RestAssured.*;

public class ConsentPage {

    public  WebDriver driver;
    public static String LoanApplicationID;

    private String responseBody;

    public ConsentPage(WebDriver driver){

        this.driver= driver;

    }

    public void getLoanApplicationID() throws InterruptedException {

        Thread.sleep(5000);
        WebElement LoanID= driver.findElement(By.xpath("//span[@class='id']"));
       LoanApplicationID = LoanID.getText();
       Thread.sleep(5000);
      System.out.print("LoanApplicationID="+ LoanApplicationID);

    }

    public void selectAggregator() throws InterruptedException {

        Thread.sleep(5000);

        driver.findElement(By.xpath("//button[normalize-space()='Select Financial Aggregator']")).click();

    }

    public void showAggregatorlist() throws InterruptedException {

        Thread.sleep(3000);

        WebElement pagename  =driver.findElement(By.xpath("//a[normalize-space()='Aggregator Selection']"));

        pagename.getText();

        Assert.assertEquals(pagename.getText(),"Aggregator Selection");
    }

    public void selectOneMoney() throws InterruptedException {

        Thread.sleep(3000);

        driver.findElement(By.xpath("//body//div[@id='root']//div[@class='row']//div//div//div[2]//div[1]//img[1]")).click();


    }

    public void aggregatorID() throws InterruptedException {

        Thread.sleep(3000);

        driver.findElement(By.xpath("//input[@placeholder='Mobile Number']")).sendKeys("1234567890");
    }

    public void initiateConsent() throws InterruptedException {

        Thread.sleep(3000);

        driver.findElement(By.xpath("//button[@type='submit']")).click();
    }

    public void approveConsent() throws InterruptedException {

        Thread.sleep(5000);

        // Perform GET request
        RestAssured.config = RestAssured.config().sslConfig(SSLConfig.sslConfig().allowAllHostnames());


        String ApiUrl = "https://dev.techbulls.io/apps/lsp/api/bridge/util/getConsentHandle/" + LoanApplicationID;

         responseBody = given()
                .when()
                .get(ApiUrl).then().statusCode(200).extract().response().asString(); // Specify endpoint



        // Print response body
        System.out.println("GET Response Body: " + responseBody);


//        here, switch to original window

        // Capture the handle of the original tab
        String originalWindowHandle = driver.getWindowHandle();
        System.out.println(driver.getCurrentUrl());
        Thread.sleep(2000);

        // Explicitly wait for the new tab to open
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        boolean isNewTabOpened = wait.until(new ExpectedCondition<Boolean>() {

            public Boolean apply(WebDriver driver) {
                return driver.getWindowHandles().size() == 2;
            }
        });

        // Switch to the new tab if it was opened
        if (isNewTabOpened) {
            Set<String> allWindowHandles = driver.getWindowHandles();
            String newTabHandle = allWindowHandles.stream().filter(handle -> !handle.equals(originalWindowHandle)).findFirst().orElse(null);
            if (newTabHandle != null) {
                driver.switchTo().window(newTabHandle);

                // Perform actions on the new tab if needed
            }
        } else {
            System.out.println("New tab not found.");
        }

        driver.switchTo().window(originalWindowHandle);





    }


    public void postApiConsentHandle() throws InterruptedException {

        RestAssured.config = RestAssured.config().sslConfig(SSLConfig.sslConfig().allowAllHostnames());


        RestAssured.baseURI= "https://dev.techbulls.io";



        // Define the JSON body
        String jsonBody = "{\n" +
                "    \"timestamp\": \"2023-11-08T16:08:06.018+05:30\",\n" +
                "    \"txId\": \"e2ec7305-cf66-44fb-8c1c-a169bfc652c0\",\n" +
                "    \"consentId\": \"" + responseBody + "\",\n" +
                "    \"FIStatus\": \"READY\",\n" +
                "    \"customerId\": \"2\"\n" +
                "}";

        System.out.println("jsonBody is =" + jsonBody);
        Thread.sleep(2000);
        // Perform POST request with response body from GET request
        String Response = given().contentType(ContentType.JSON) // Set content type
                .body(jsonBody) // Use response body from GET request
                .when()
                .post("/apps/lsp/api/bridge/FIU/Notification/Status")
                .then().statusCode(200).extract().asString(); // Specify endpoint for POST request
        Thread.sleep(5000);


        System.out.println("Post request response="+Response);

    }

    public void offersPage() throws InterruptedException {

        Thread.sleep(5000);

       WebElement offersPage = driver.findElement(By.xpath("//div[@class='container-main-heading']"));
       Assert.assertEquals(offersPage.getText(),"Credit Offers");


    }
}
