package pages;

import Utils.CommonFunctions;
import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static io.restassured.RestAssured.*;

public class RepaymentPage {

    private final WebDriver driver;

   public String RepayResponse;

   public static String MainWindow;

    public RepaymentPage(WebDriver driver) {

        this.driver=driver;
    }

    public void selectRepaymentPlan() throws InterruptedException {


        Thread.sleep(5000);
        driver.findElement(By.xpath("//div[@class='repaymentPlan-card card-body']")).click();



    }

public void proceedWithPlan() throws InterruptedException {


    Thread.sleep(5000);
    MainWindow = driver.getWindowHandle();

//    count no of cards
//  Xpath=  //div[@class='repaymentPlan-card card-body']

  List<WebElement> cards =  driver.findElements(By.xpath("//div[@class='repaymentPlan-card card-body']"));
  int NoOfCards= cards.size();
  System.out.println("No of repayment plans="+ NoOfCards);


      CommonFunctions.scrollIntoView(driver.findElement(By.xpath("//button[@type='submit']")));
//    JavascriptExecutor js= (JavascriptExecutor) driver;
//    js.executeScript("window.scrollBy(0, 1100)");
    Thread.sleep(3000);

    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(20000);


}

public void navigateTodisbursementScreen() throws InterruptedException {




  driver.switchTo().window(MainWindow);

  Thread.sleep(5000);
    RestAssured.config = RestAssured.config().sslConfig(SSLConfig.sslConfig().allowAllHostnames());

  String  RepayUrl ="https://dev.techbulls.io/apps/lsp/api/bridge/rzpay/updateRepaymentPlanStatusWebhook";

  String jsonBody="{\n" +
          "    \"loanApplicationId\": \""+ConsentPage.LoanApplicationID+"\",\n" +
          "    \"status\": \"ACTIVE\"\n" +
          "}";
  Thread.sleep(5000);
    RepayResponse = given().contentType(ContentType.JSON).body(jsonBody).when().post(RepayUrl).then()
            .statusCode(200)
            .extract().asString();

    System.out.print("Repay API Response is="+ RepayResponse);

    Thread.sleep(3000);

    WebElement disburseLoan= driver.findElement(By.xpath("//div[@class='main-heading row']"));
    Assert.assertEquals(disburseLoan.getText(), "Secure Loan Disbursement Setup");
}
}
