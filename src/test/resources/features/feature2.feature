Feature: Upload documents


  @Smoke
  Scenario: Initiate loan journey
    When User click on button InitiateLoanApplication
    Then upload documents page should get open


    @Smoke
    Scenario Outline:  Upload a pledge docs
    When user clicks on doc Type dropdown and select a value PAN to "<UploadFileType>"
    And  user click on a browse to upload a file "<UploadFile>"
    Then user click on upload button
    Examples:
      | UploadFileType | UploadFile |
      |PAN         |/home/techbulls/Pictures/extradots.png|
      |PASSPORT    |/home/techbulls/Pictures/repaymentplan.png|

    Scenario Outline: upload a borrower docs
      When user click on dropdown for "<fileType>"
      And he click on browse to upload a "<file>"
      Then user click on upload button of borrower
      Examples:
        | fileType | file |
        |AADHAAR   |/home/techbulls/Pictures/extradots.png|
        |PAN       |/home/techbulls/Pictures/extradots.png|







