Feature: Choose guarantor & applicant will be same as borrower

  Scenario: Check Checkboxes
    When user scroll down the page
    And  user check the checkboxes of guarantor & applicant
    And  user click on Save button
    Then user should navigate to review page

    Scenario: Submit the application
      When user click on submit application button
      Then user should navigate to consent handle page
