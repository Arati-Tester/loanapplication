Feature: Start Loan Journey

  @Sanity
  Scenario Outline: Add loan Amount Details
    Given user is on homepage
    When user enter a "<RequestAmount>" and "<Duration>"
    And user select a "<ProfileType>"
    And user click on button borrower Details
    Then user should navigate to basic details page

    Examples:
      | RequestAmount | Duration | ProfileType |
      |45000          |3         |INDIVIDUAL   |


    Scenario Outline: Add Basic Details
      When  user enter a "<FullName>" and "<Email>" and "<Mobile>" and "<Birthdate>" and "<primaryid>"
      And   user click on button Address
      Then  user successfully landed on address page
      Examples:
        |FullName|Email|Mobile|Birthdate|primaryid|
        |Arti S |arts@gmail.com|9879879878|10/09/2000 |TGFYH6547B|


      Scenario Outline: Add Address Details
        When  user enter a address details "<pincode>" and "<address>"
        And   clicking on button Check Eligibilty
        Then  user should navigate to congratulations screen
        Examples:
          |pincode|address|
          |411001 |vadgaon Sheri|


