Feature: Select a Offer & KYC status

 Scenario Outline: Offer selection
    When user click on any generated offer
    Then Action required page will be open
    When user upload a all required "<File type>" and "<File Name>"
    Then user again navigate to same offer page
    When user again select a offer
    Then Confirmation Pop-up will be open & confirm the same
    Then User navigate to KYC details
    When User click on initiate KYC
    Then User navigate to KYC page
   Examples:
     | File type | File Name |
     |AADHAAR   |/home/techbulls/Pictures/extradots.png|



   Scenario: Approve loan agreement
     When user navigate back to KYC Status page And click on KYC Status
     Then user navigate to loan aggrement page
     When user click on Accept agreement & continue
     Then user should get a OTP pop-up
     And user confirm the agreement
     And user landed on repayment plan page

