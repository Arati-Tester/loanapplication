Feature: Consent Handle

  Scenario: Select financial aggregator
    When user click on button Aggregator Selection
    Then user navigate to aggregator list page
    When user click on aggregator OneMoney
    And  Enter a aggregator ID
    When user click on InitiateConsent button
    And  user has to approve consent from API
    Then user navigate to Offers page


