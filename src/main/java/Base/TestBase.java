package Base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.WebDriver;

public class TestBase {
        public static WebDriver driver;

        public static WebDriver getDriver() {
            if (driver == null) {

                WebDriverManager.chromedriver().setup();
                ChromeOptions options= new ChromeOptions();
//                options.addArguments("--remote-allow-origins=*");
                options.addArguments("--ignore-certificate-errors");
                options.addArguments("--headless=new");


                driver = new ChromeDriver(options);
//                options.addArguments("location.reload(true)");



            }
            return driver;
        }




}
