package Utils;

import Base.TestBase;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonFunctions extends TestBase {



//    public static void scrollDown(int ScrollBy){
//
//        JavascriptExecutor js= (JavascriptExecutor) driver;
//        js.executeScript("windows.scrollBy(0, ScrollBy)");
//    }


    public static void scrollIntoView(WebElement element) {

        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
//        element.isDisplayed();

    }


}
